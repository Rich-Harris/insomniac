# insomniac

Prevent browser from falling asleep on iOS and Android. Basically identical to https://github.com/richtr/NoSleep.js, except with a slightly leaner API, and UMD and ES module builds. (Thanks to [Rich Tibbett](https://github.com/richtr) for making the original code available under an [MIT license](https://github.com/richtr/NoSleep.js#license).)

## Installation and usage

```bash
npm i insomniac
```

...or grab it from [npmcdn.com/insomniac](https://npmcdn.com/insomniac).

```js
import * as insomniac from 'insomniac'; // or `var insomniac = require('insomniac')` if you're old-fashioned

// Enable wake lock.
// (must be wrapped in a user input event handler e.g. a mouse or touch handler)
document.addEventListener('touchstart', insomniac.enable, false);

// ...

// Disable wake lock at some point in the future.
// (does not need to be wrapped in any user input event handler)
insomniac.disable();
```

## License

MIT. Modifications (c) Rich Harris, original code (c) Rich Tibbett (no relation...)
