import buble from 'rollup-plugin-buble';

export default {
	entry: 'src/index.js',
	plugins: [ buble() ],
	moduleName: 'insomniac',
	targets: [
		{ format: 'umd', dest: 'dist/insomniac.umd.js' },
		{ format: 'es', dest: 'dist/insomniac.es.js' }
	]
};
